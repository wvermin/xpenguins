#!/bin/sh
# -copyright-
#-# 
#-# Copyright (C) 1999-2001  Robin Hogan, 2021-2024 Willem Vermin
#-# 
#-# This program is free software; you can redistribute it and/or modify
#-# it under the terms of the GNU General Public License as published by
#-# the Free Software Foundation; either version 2 of the License, or
#-# (at your option) any later version.
#-# 
#-# This program is distributed in the hope that it will be useful,
#-# but WITHOUT ANY WARRANTY; without even the implied warranty of
#-# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#-# GNU General Public License for more details.
#-# 
#-# You should have received a copy of the GNU General Public License
#-# along with this program; if not, write to the Free Software
#-# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#-# 
cat << eof > toascii.c
#include <stdio.h>
int main()
{
   int c;
   int i = 0;
   while((c = getchar()) != EOF)
   {
      if (i>20)
      {
	 i = 1;
	 printf("\n");
      }
      printf("%d,",c);
      i++;
   }
}
eof
${CC:-cc} $CFLAGS -o toascii toascii.c >/dev/null 2>&1 
./toascii
rm -f toascii toascii.c


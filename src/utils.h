/* -copyright-
#-# 
#-# Copyright (C) 1999-2001  Robin Hogan, 2021-2024 Willem Vermin
#-# 
#-# This program is free software; you can redistribute it and/or modify
#-# it under the terms of the GNU General Public License as published by
#-# the Free Software Foundation; either version 2 of the License, or
#-# (at your option) any later version.
#-# 
#-# This program is distributed in the hope that it will be useful,
#-# but WITHOUT ANY WARRANTY; without even the implied warranty of
#-# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#-# GNU General Public License for more details.
#-# 
#-# You should have received a copy of the GNU General Public License
#-# along with this program; if not, write to the Free Software
#-# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#-# 
*/
#pragma once

//#define add_to_mainloop(prio,time,func,datap) g_timeout_add_full(prio,(int)1000*(time),(GSourceFunc)func,datap,0)


#define FLAGSFILE ".xpenguinsrc"

#include <stdio.h>
#include <X11/Intrinsic.h>
#include <gtk/gtk.h>
#include <stdlib.h>
#include <math.h>

#define myexp10f(x) (expf(2.3025850930f*x))
#define mylog10f(x) (0.4342944819f*logf(x))

extern float   fsignf(float x);
extern FILE   *HomeOpen(const char *file,const char *mode,char **path);
extern float   sq2(float x, float y);
extern float   sq3(float x, float y, float z);
extern void    my_cairo_paint_with_alpha(cairo_t *cr, double alpha);
extern int     RandInt(int maxint);
extern Window  Window_With_Name( Display *dpy, Window top, const char *name);
extern double  wallclock(void);
extern double  wallcl(void);
extern void    mystrncpy(char *dest, const char *src, size_t n);

extern Pixel   Black, White;

extern int   is_little_endian(void);
extern int   sgnf(float x);
extern float fsgnf(float x);

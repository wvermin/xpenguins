#!/bin/sh
# -copyright-
#-# 
#-# Copyright (C) 1999-2001  Robin Hogan, 2021-2024 Willem Vermin
#-# 
#-# This program is free software; you can redistribute it and/or modify
#-# it under the terms of the GNU General Public License as published by
#-# the Free Software Foundation; either version 2 of the License, or
#-# (at your option) any later version.
#-# 
#-# This program is distributed in the hope that it will be useful,
#-# but WITHOUT ANY WARRANTY; without even the implied warranty of
#-# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#-# GNU General Public License for more details.
#-# 
#-# You should have received a copy of the GNU General Public License
#-# along with this program; if not, write to the Free Software
#-# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#-# 
# create C code to get ui.xml in a string
# ISO C stipulates that the length of a string constant should
# not be larger than 4096, so we create a definition as in
# char xpenguins_xml[] = {60,63,120,109,108,32,118,101,0};
#
root="${1:-..}"
in="ui.xml"
out="ui_xml.h"
echo "/* This file is generated from '$in' by '$0' */" > "$out"
echo "/* -copyright-" >> "$out"
echo "*/" >> "$out"
echo "#pragma once" >> "$out"
echo "char xpenguins_xml[] = {" >> "$out"
sed 's/^ *//' "$root/src/$in" | awk -v FS="" \
   'BEGIN{for(n=0;n<256;n++)ord[sprintf("%c",n)]=n;}
   {for (i=1;i<=NF;i++) printf "%d,", ord[$i];
      printf "%d,\n",ord["\n"];}' >> "$out"
echo "0};">> "$out" 
if [ -x "$root/addcopyright.sh" ] ; then "$root/addcopyright.sh" "$out" ; fi

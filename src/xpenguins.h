/* xpenguins.h - functions for animating penguins in your root window
 * -copyright-
#-# 
#-# Copyright (C) 1999-2001  Robin Hogan, 2021-2024 Willem Vermin
#-# 
#-# This program is free software; you can redistribute it and/or modify
#-# it under the terms of the GNU General Public License as published by
#-# the Free Software Foundation; either version 2 of the License, or
#-# (at your option) any later version.
#-# 
#-# This program is distributed in the hope that it will be useful,
#-# but WITHOUT ANY WARRANTY; without even the implied warranty of
#-# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#-# GNU General Public License for more details.
#-# 
#-# You should have received a copy of the GNU General Public License
#-# along with this program; if not, write to the Free Software
#-# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#-# 
*/

#include <stdio.h>

#include "toon.h"

#define PENGUIN_MAX 512

#define PENGUIN_DEFAULTWIDTH 30
#define PENGUIN_DEFAULTHEIGHT 30

#define PENGUIN_FORWARD 0
#define PENGUIN_LEFTRIGHT 1
#define PENGUIN_LEFT 0
#define PENGUIN_RIGHT 1

enum{
   PENGUIN_WALKER,     /* Walking along the tops of windows */
   PENGUIN_FALLER,     /* Falling from the top of the screen */
   PENGUIN_TUMBLER,    /* After falling off a window */
   PENGUIN_FLOATER,    /* Well, flying really */
   PENGUIN_CLIMBER,    /* Climbing up the sides of windows or the screen */
   PENGUIN_EXIT,       /* Exit sequence */
   PENGUIN_EXPLOSION,  /* Simple explosion with NO BLOOD! */
   PENGUIN_RUNNER,     /* Just like walker but usually faster */
   PENGUIN_SPLATTED,   /* Sometimes when falling onto a hard surface */
   PENGUIN_SQUASHED,   /* When caught under windows */
   PENGUIN_ZAPPED,     /* Zapped by mouse pointer (not yet used) */
   PENGUIN_ANGEL,      /* After some nasty death, floating upwards */
   PENGUIN_ACTION0,    /* Reading, sleeping, jumping, whatever */
   PENGUIN_ACTION1,    /* The ACTIONs must have consecutive numbers */
   PENGUIN_ACTION2, 
   PENGUIN_ACTION3,
   PENGUIN_ACTION4,
   PENGUIN_ACTION5
};


#define PENGUIN_NGENERA 2   /* Number of genera allocated, not the max possible */
#define PENGUIN_NTYPES 18   /* walker, faller ... */
#define PENGUIN_NACTIONS 6

#define PENGUIN_JUMP 8

enum{
   PENGUIN_ARTIST     = 1,
   PENGUIN_MAINTAINER,
   PENGUIN_DATE,
   PENGUIN_COPYRIGHT,
   PENGUIN_LICENSE,
   PENGUIN_ICON,
   PENGUIN_COMMENT,
   PENGUIN_NABOUTFIELDS
};

extern const char *NumToType[];
#define XPENGUINS_DEBUG fprintf(stderr, __FILE__ ": line %d\n", __LINE__)

/*
 * Themes are searched for in the following locations:
 * $HOME/.xpenguins/themes
 * [xpenguins_directory]/themes
 */
#ifndef XPENGUINS_SYSTEM_DIRECTORY
#ifdef PKGDATADIR
#define XPENGUINS_SYSTEM_DIRECTORY PKGDATADIR
#else
#define XPENGUINS_SYSTEM_DIRECTORY "/usr/share/xpenguins"
#endif

#endif
#ifndef XPENGUINS_USER_DIRECTORY
#define XPENGUINS_USER_DIRECTORY "/.xpenguins"
#endif

#ifndef XPENGUINS_THEME_DIRECTORY
#define XPENGUINS_THEME_DIRECTORY "/themes"
#endif

#ifndef XPENGUINS_CONFIG
#define XPENGUINS_CONFIG "/config"
#endif

/* The XPenguinsTheme structure contains all the information about the toon,
 *  basically an array of ToonData structures */

typedef struct {
   ToonData **data;      /* array of pointers to peguin data  0..ngenera-1, genus == pinguin type */
   char **name;          /* names of genera */
   int *number;          /* array of numbers denoting number of penguins of this type on the screen */
   int total;            /* Sum of "number", but not more than PENGUIN_MAX */
   int ngenera;          /* number of different penguins */
   int delay;            /* delay in msecs */
   int _nallocated;      /* number of allocated genera */
} XPenguinsTheme;

extern char      *xpenguins_directory;
extern ToonData **penguin_data;
extern int        penguin_ngenera;
extern int       *penguin_numbers;
extern int        penguin_number;
extern int        xpenguins_verbose;
extern int        xpenguins_blood;
extern int        xpenguins_angels;
extern int        xpenguins_active;

extern void ToonPrint(void);
extern void CheckPenguinsY(void);

/*** FUNCTIONS in xpenguins_core.c ***/

/* Contact X server and upload the data in `penguin_data'.
 * On success NULL is returned, otherwise a pointer to a static
 * message indicating the fault is returned.  */
extern char *xpenguins_start(char *display_name);
extern int  default_penguin_number(void);
extern Toon *getpenguin(int i);

/* Set the required number of toons to animate. If the toons are
 * active and the number is less than the current number then the
 * excess will be turned into exiters or explosions and their
 * terminating flag will be set. */
void xpenguins_set_number(int n);

/* Ignore or observe `popup' windows */
void xpenguins_ignorepopups(int yn);

/* Advance the toons by one frame. Returns the number of penguins that
 * are active or not terminating i.e. when 0 is returned, we can call
 * xpenguins_exit(). */
int xpenguins_frame();

/* Don't advance penguins, but check if they have been uncovered by 
 * moving windows and need to be redrawn. */
void xpenguins_pause_frame();

void ToonDataPrint(ToonData *p);

/* The command-line version of xpenguins will want to call this to do
 * the delays between frames, while the GNOME version will use its own
 * callbacks. */
#define xpenguins_sleep ToonSleep

/* Turn all the penguins into exiters and set their terminating flag */
#define xpenguins_terminate() xpenguins_set_number(0)

/* draw all penguins */
void xpenguins_draw(void);


/*** FUNCTIONS in xpenguins_theme.c ***/

void xpenguins_print_list(char **list);
/* Set the data directory to use to find themes */
void xpenguins_set_directory(char *directory);

/* Returns an array of strings the theme names - this list is
 * dynamically allocated so should be freed using
 * xpenguins_free_list(), unless NULL is returned, in which case no
 * themes were found. The number of themes is returned in n. */
char **xpenguins_list_themes(int *n);

/* Generic function for freeing character lists that are allocated for
 * verious purposes. */
void xpenguins_free_list(char **list);

/* Returns a string containing the directory where the theme called
 * `name' can be found - this string should be freed using free(),
 * unless NULL is returned, which indicates that the requested theme
 * could not be found.  */
char *xpenguins_theme_directory(char *name);

/* Loads a theme into a pre-existing structure, returns 0 on success,
 * a (statically-allocated) string containing the error on failure. */
//char *xpenguins_load_theme(char *name, XPenguinsTheme *theme);

char *xpenguins_load_themes(char ***names, XPenguinsTheme *theme);

/* Free all the data associated with a theme */
void xpenguins_free_theme(XPenguinsTheme *theme);

/* Print basic theme information to standard error - for debugging
 * purposes */
void xpenguins_describe_theme(XPenguinsTheme *theme);

/* Read the `about' file in a theme directory and return a string list
 * of theme properties. This list should be freed with
 * xpenguins_free_list(). */
char **xpenguins_theme_info(char *name);

/* Remove underscores from a theme name */
char *xpenguins_remove_underscores(char *themename);

/* Don't show any nasty deaths */
#define xpenguins_set_blood(yn) xpenguins_blood = yn

/* Don't show angels flying up to heaven */
#define xpenguins_set_angels(yn) xpenguins_angels = yn


/*** FUNCTIONS in xpenguins_config.c ***/

/* Read the next word from a config file into buf */
int xpenguins_read_word(FILE *file, int max_size, char *buf);

/* Read a line from a config file */
int xpenguins_read_line(FILE *file, int max_size, char *buf);

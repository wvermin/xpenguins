/* -copyright-
#-# 
#-# Copyright (C) 1999-2001  Robin Hogan, 2021-2024 Willem Vermin
#-# 
#-# This program is free software; you can redistribute it and/or modify
#-# it under the terms of the GNU General Public License as published by
#-# the Free Software Foundation; either version 2 of the License, or
#-# (at your option) any later version.
#-# 
#-# This program is distributed in the hope that it will be useful,
#-# but WITHOUT ANY WARRANTY; without even the implied warranty of
#-# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#-# GNU General Public License for more details.
#-# 
#-# You should have received a copy of the GNU General Public License
#-# along with this program; if not, write to the Free Software
#-# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#-# 
 */
#include <stdio.h>
#include "docs.h"
#include "xpenguins.h"


void docs_usage(char **argv)
{
   printf(_("Usage: %s [options]\n"), argv[0]);
   printf(_("Options:\n"
	    "  -d, --display        <display>    Send the penguins to specified display\n"
	    "  -m, --delay          <millisecs>  Set delay between frames\n"));
   printf(_("  -n, --penguins       <n>          Create <n> penguins (max %d)\n"),
	 PENGUIN_MAX);
   printf(_("  -q, --quiet                       Suppress all non-fatal messages\n"
	    "  -v, --version                     Show version information\n"
	    "  -h, --help                        Show this message\n"
	    "      --defaults                    Skip reading from ~/.xpenguinrc\n"
	    "      --nomenu                      Do not show menu\n"
	    "      --hidemenu                    Iconify menu at startup\n"
	    "      --nodoublebuffer              Do not use double buffering\n"
	    "  -c, --config-dir     <dir>        Look for config files (and themes) in <dir>\n"
	    "  -p, --ignorepopups                Penguins ignore \"popup\" windows\n"
	    "  -r, --rectwin                     Regard shaped windows as rectangular\n"
	    "  -t, --theme          <theme>      Use named <theme>\n"
	    "  -l, --list-themes                 List available themes\n"
	    "  -i, --theme-info                  Describe a theme and exit (use with -t)\n"
	    "  -b, --no-blood                    Do not show any gory images\n"
	    "  -a, --no-angels                   Do not show any cherubim\n"
	    "  -s, --squish                      kill penguins with mouse\n"
	    "      --lift           <n>          Lift penguins window n pixels\n"
	    "      --all                         Run all available themes simultaneously\n"
	    "      --random-theme                Choose a random theme\n"
	    "      --id             <window id>  Send penguins to window with this ID\n"
	    "      --nice           <ld1> <ld2>  Start killing penguins when load reaches\n"
	    "                                       <ld1>, kill all if load reches <ld2>\n"
	    "      --changelog                   Show ChangeLog\n"
	    "      --selfrep                     Output source as a gzipped tar file\n"
	    "(\"--\" can be replaced with \"-\" in all cases)\n"
	    "System data directory: "));
   printf(PKGDATADIR "\n");
}

void docs_changelog()
{
#include "changelog.inc"
}


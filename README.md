# xpenguins

A revival of the famous xpenguins program. Let penguins populate your desktop.

## installation:

````
cd xpenguins
./configure
make
sudo make install
````
